package com.sistema.vendas.dtos;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.boot.test.context.SpringBootTest;

import com.sistema.vendas.address.city.City;
import com.sistema.vendas.address.city.CityDTO;

@SpringBootTest
@TestInstance(Lifecycle.PER_CLASS)
class CityDTOTest{

	private CityDTO dto;

	@BeforeAll
	void setup(){
		dto = new CityDTO(1L, "CE", "Pacatuba");
	}

	@Test
	@DisplayName("Novo City DTO")
	void a(){
		assertEquals(dto.getClass(), CityDTO.class);
	}

	@Test
	@DisplayName("Equals com objeto igual")
	void b(){
		CityDTO cityDTO = dto;
		boolean equal = dto.equals(cityDTO);
		assertTrue(equal);
	}

	@Test
	@DisplayName("Equals com objeto diferente")
	void c(){
		CityDTO cityDto = new CityDTO(1L, "CE", "Pacatuba");
		assertEquals(cityDto, dto);
	}

	@Test
	@DisplayName("Equals com objeto de classe diferente")
	void d(){
		CityDTO cityDTO = new CityDTO();
		assertNotEquals(cityDTO, dto);
	}

	@Test
	@DisplayName("HasCode")
	void e(){
		Integer hash = dto.hashCode();
		assertEquals(Integer.class, hash.getClass());
	}

	@Test
	@DisplayName("GetID")
	void f(){
		assertEquals(1L, dto.getId());
	}

	@Test
	@DisplayName("Convertendo DTO para Entidade")
	void g(){
		City city = CityDTO.convertToEntity(dto);
		assertEquals(1L, city.getId());
	}

	@Test
	@DisplayName("Convertendo Entidade para DTO")
	void h(){
		CityDTO cdto = CityDTO.convertToDto(new City(2L, "PB", "Campina Grande"));
		assertEquals(2L, cdto.getId());
	}

}
