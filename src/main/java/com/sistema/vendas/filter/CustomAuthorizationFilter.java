package com.sistema.vendas.filter;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static java.util.Arrays.stream;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Slf4j
public class CustomAuthorizationFilter extends OncePerRequestFilter{

  private final String secret;

  public CustomAuthorizationFilter(String secret){
	this.secret = secret;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException{
	if(request.getServletPath().equals("/api/login")){
	  filterChain.doFilter(request, response);
	}else{
	  try{
		isAuthorized(request, response, filterChain);
	  }catch(JWTDecodeException e){
		getMessage(response, e, HttpStatus.BAD_REQUEST, "Token inválido");
	  }catch(TokenExpiredException e){
		getMessage(response, e, HttpStatus.FORBIDDEN, "Token Expirado");
	  }catch(DataAccessException e){
		getMessage(response, e, HttpStatus.NOT_FOUND, "Data Access Exceptino");
	  }catch(Exception e){
		getMessage(response, e, HttpStatus.INTERNAL_SERVER_ERROR, "Exception");
	  }
	}
  }

  private void isAuthorized(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException{
	String authorizationHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
	if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
	  getAuthorization(request, response, filterChain, authorizationHeader);
	}else{
	  filterChain.doFilter(request, response);
	}
  }

  private void getAuthorization(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain, String authorizationHeader) throws IOException, ServletException{

	String token = authorizationHeader.substring("Bearer ".length());
	Algorithm algorithm = Algorithm.HMAC256(secret.getBytes());
	JWTVerifier verifier = JWT.require(algorithm).build();
	DecodedJWT decoded = verifier.verify(token);
	String username = decoded.getSubject();
	String[] roles = decoded.getClaim("roles").asArray(String.class);
	Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
	stream(roles).forEach(role->authorities.add(new SimpleGrantedAuthority(role)));
	UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, null,
																									  authorities);
	SecurityContextHolder.getContext().setAuthentication(authenticationToken);
	filterChain.doFilter(request, response);
  }

  private void getMessage(HttpServletResponse response, Exception exception, HttpStatus status, String developerMessage) throws IOException{

	Map<String, String> error = new HashMap<>();

	error.put("timestamp", LocalDateTime.now().toString());
	error.put("status", status.toString());
	error.put("statusCode", String.valueOf(status.value()));

	if(exception.getClass() != null){
	  error.put("type", exception.getClass().toString());
	}

	if(!exception.getMessage().isEmpty()){
	  error.put("detail", exception.getMessage());
	}

	error.put("developerMessage", developerMessage);

	if(exception.getCause() != null){
	  error.put("cause", exception.getCause().toString());
	}

	log.debug(exception.getMessage());

	response.setStatus(status.value());
	response.setContentType(MediaType.APPLICATION_JSON_VALUE);

	new ObjectMapper().writeValue(response.getOutputStream(), error);
  }

}
