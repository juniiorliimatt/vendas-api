package com.sistema.vendas.exception;

import org.springframework.core.NestedRuntimeException;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

public class RefreshTokenMissingException extends NestedRuntimeException{

	private static final long serialVersionUID = -4206371294220105633L;

	public RefreshTokenMissingException(String msg){
		super(msg);
	}

}
