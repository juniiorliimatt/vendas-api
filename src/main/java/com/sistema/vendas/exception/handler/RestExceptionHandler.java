package com.sistema.vendas.exception.handler;

import com.auth0.jwt.exceptions.JWTDecodeException;
import com.sistema.vendas.exception.RefreshTokenMissingException;
import com.sistema.vendas.exception.RoleNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

@RestControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{

  @Override
  public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException e, HttpHeaders headers, HttpStatus status, WebRequest request){
	return new ResponseEntity<>(ErrorResponse.builder()
											 .status(HttpStatus.BAD_REQUEST.toString())
											 .statusCode(HttpStatus.BAD_REQUEST.value())
											 .type(MethodArgumentNotValidException.class.toString())
											 .detail(e.getMessage())
											 .developerMessage(e.getLocalizedMessage())
											 .message("Requisição possui campos inválidos")
											 .timestamp(LocalDateTime.now())
											 .objectName(e.getBindingResult()
														  .getObjectName())
											 .errors(getErrors(e))
											 .build(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(JWTDecodeException.class)
  public ResponseEntity<ErrorResponse> handleJWTDecodeException(JWTDecodeException e){
	return new ResponseEntity<>(ErrorResponse.builder()
											 .statusCode(HttpStatus.BAD_REQUEST.value())
											 .type(JWTDecodeException.class.toString())
											 .detail(e.getMessage())
											 .developerMessage(e.getLocalizedMessage())
											 .message("Token inválido")
											 .status(HttpStatus.BAD_REQUEST.toString())
											 .timestamp(LocalDateTime.now())
											 .objectName(e.getClass()
														  .toString())
											 .build(), HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(RoleNotFoundException.class)
  public ResponseEntity<ErrorResponse> handleRoleNotFoundException(RoleNotFoundException e){
	return new ResponseEntity<>(ErrorResponse.builder()
											 .statusCode(HttpStatus.NOT_FOUND.value())
											 .type(JWTDecodeException.class.toString())
											 .detail(e.getMessage())
											 .developerMessage(e.getLocalizedMessage())
											 .message("Role não encontrada")
											 .status(HttpStatus.NOT_FOUND.toString())
											 .timestamp(LocalDateTime.now())
											 .build(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(RefreshTokenMissingException.class)
  public ResponseEntity<ErrorResponse> handleTokenExpiredException(RefreshTokenMissingException e){
	return new ResponseEntity<>(ErrorResponse.builder()
											 .statusCode(HttpStatus.NOT_FOUND.value())
											 .type(JWTDecodeException.class.toString())
											 .detail(e.getMessage())
											 .developerMessage(e.getLocalizedMessage())
											 .message("Erro Refresh Token")
											 .status(HttpStatus.NOT_FOUND.toString())
											 .timestamp(LocalDateTime.now())
											 .build(), HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler(TransactionSystemException.class)
  public ResponseEntity<ErrorResponse> handleTransactionSystemException(TransactionSystemException e){
	return new ResponseEntity<>(ErrorResponse.builder()
											 .status(HttpStatus.BAD_REQUEST.toString())
											 .statusCode(HttpStatus.BAD_REQUEST.value())
											 .type(TransactionSystemException.class.toString())
											 .detail(e.getMostSpecificCause()
													  .toString())
											 .developerMessage(e.getCause()
																.toString())
											 .message("Erro de validação")
											 .timestamp(LocalDateTime.now())
											 .build(), HttpStatus.BAD_REQUEST);
  }

  private List<Error> getErrors(MethodArgumentNotValidException ex){
	return ex.getBindingResult()
			 .getFieldErrors()
			 .stream()
			 .map(error->new Error(error.getDefaultMessage(), error.getField(), error.getRejectedValue()))
			 .collect(Collectors.toList());
  }
}
