package com.sistema.vendas.exception.handler;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;
import java.util.List;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Getter
@AllArgsConstructor
@SuperBuilder
@JsonInclude(NON_NULL)
public class ErrorResponse{

	private int statusCode;
	private String type;
	private String detail;
	private String developerMessage;
	private LocalDateTime timestamp;
	private String message;
	private String status;
	private String objectName;
	private List<Error> errors;

}
