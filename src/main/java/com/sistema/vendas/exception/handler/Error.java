package com.sistema.vendas.exception.handler;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

@Getter
@AllArgsConstructor
public class Error{

	private final String message;
	private final String field;
	private final Object parameter;

}
