package com.sistema.vendas.exception;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

public class RoleNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 64591124106040931L;

	public RoleNotFoundException(String message){
		super(message);
	}

	public RoleNotFoundException(String message, Throwable cause){
		super(message, cause);
	}

}
