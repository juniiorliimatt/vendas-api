package com.sistema.vendas.shippingcompany;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 09/12/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class ShippingCompanyServiceImpl implements ShippingCompanyService{

	private final ShippingCompanyRepository repository;

	@Override
	@Transactional(readOnly = true)
	public List<ShippingCompany> findAll(){
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<ShippingCompany> findById(Long id){
		return repository.findById(id);
	}

	@Override
	public ShippingCompany save(ShippingCompany shippingCompany){
		return repository.save(shippingCompany);
	}

	@Override
	public ShippingCompany update(ShippingCompany shippingCompany){
		return repository.save(shippingCompany);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ShippingCompany> findByNameStartingWithIgnoreCase(String name){
		return repository.findByNameStartingWithIgnoreCase(name);
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<ShippingCompany> findByCnpjLike(String cnpj){
		return repository.findByCnpjLike(cnpj);
	}

}
