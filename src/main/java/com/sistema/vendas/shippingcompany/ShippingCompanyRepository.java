package com.sistema.vendas.shippingcompany;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 09/12/2021
 */

@Repository
public interface ShippingCompanyRepository extends JpaRepository<ShippingCompany, Long>{

	List<ShippingCompany> findByNameStartingWithIgnoreCase(String name);

	Optional<ShippingCompany> findByCnpjLike(String cnpj);

}
