package com.sistema.vendas.shippingcompany;

import static java.util.List.of;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 09/12/2021
 */

@RestController
@RequestMapping("/api/v1/shipping-company")
@RequiredArgsConstructor
@Slf4j
public class ShippingCompanyController{

	private final ShippingCompanyService service;
	private static final String TRANSPORTADORAS_RECUPERADAS = "Transportadoras recuperadas";
	private static final String TRANSPORTADORA_RECUPERADA = "Transportadora recuperada";
	private static final String TRANSPORTADORA = "Transportadora";

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		List<ShippingCompany> shippingCompanies = service.findAll();
		List<ShippingCompanyDTO> carrierDTOS = new ArrayList<>();
		shippingCompanies.forEach(carrier->carrierDTOS.add(ShippingCompanyDTO.convertToDto(carrier)));
		log.info(TRANSPORTADORAS_RECUPERADAS);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(carrierDTOS))
																			 .message(TRANSPORTADORAS_RECUPERADAS)
																			 .status(HttpStatus.OK)
																			 .statusCode(HttpStatus.OK.value())
																			 .build());
	}

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		Optional<ShippingCompany> shippingcompany = service.findById(id);
		return findShippingCompany(shippingcompany);
	}

	@GetMapping("/find/name/{name}")
	public ResponseEntity<Response> findByPersonalData_NameStartingWithIgnoreCase(@PathVariable("name") String name){
		List<ShippingCompany> companies = service.findByNameStartingWithIgnoreCase(name);
		return findCarriers(companies);
	}

	@GetMapping("/find/cnpj/{cnpj}")
	public ResponseEntity<Response> findByPersonalData_CpfLike(@PathVariable("cnpj") String cnpj){
		Optional<ShippingCompany> shippingCompany = service.findByCnpjLike(cnpj);
		return findShippingCompany(shippingCompany);
	}

	@PostMapping("/save")
	public ResponseEntity<Response> save(@RequestBody ShippingCompanyDTO dto){
		ShippingCompany shippingCompany = ShippingCompanyDTO.convertToEntity(dto);
		ShippingCompany save = service.save(shippingCompany);
		ShippingCompanyDTO shippingCompanyDTO = ShippingCompanyDTO.convertToDto(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/shipping-company/find/id/{id}")
																							.buildAndExpand(shippingCompanyDTO.getId())
																							.toUri();
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(of(shippingCompanyDTO))
																			 .message(TRANSPORTADORA_RECUPERADA)
																			 .status(CREATED)
																			 .statusCode(CREATED.value())
																			 .build());
	}

	@PutMapping("/update")
	public ResponseEntity<Response> update(@Valid @RequestBody ShippingCompanyDTO dto){
		ShippingCompany shippingCompany = ShippingCompanyDTO.convertToEntity(dto);
		ShippingCompany updated = service.update(shippingCompany);
		ShippingCompanyDTO shippingCompanyDTO = ShippingCompanyDTO.convertToDto(updated);
		log.info("Transportadora atualizazdo com sucesso");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(of(TRANSPORTADORA, shippingCompanyDTO))
																			 .message(TRANSPORTADORA_RECUPERADA)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> delete(@PathVariable("id") Long id){
		service.delete(id);
		log.info("Transportadora deletada com sucesso");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message("Transportadora deletada")
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	private ResponseEntity<Response> findShippingCompany(Optional<ShippingCompany> shippingCompany){
		if(shippingCompany.isEmpty()){
			log.warn("Trasnportadora não encontrado");
			return ResponseEntity.notFound()
													 .build();
		}
		ShippingCompanyDTO shippingCompanyDTO = ShippingCompanyDTO.convertToDto(shippingCompany.get());
		log.info(TRANSPORTADORA_RECUPERADA);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(of(TRANSPORTADORA, shippingCompanyDTO))
																			 .message(TRANSPORTADORA_RECUPERADA)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	private ResponseEntity<Response> findCarriers(List<ShippingCompany> carriers){
		List<ShippingCompanyDTO> shippingCompanyDTOs = new ArrayList<>();
		carriers.forEach(shippingCompany->shippingCompanyDTOs.add(ShippingCompanyDTO.convertToDto(shippingCompany)));
		if(shippingCompanyDTOs.isEmpty()){
			log.warn("Transportadoras não encontradas");
		}else{
			log.info("Transportadoras encontradas");
		}
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(of("Transportadoras", shippingCompanyDTOs))
																			 .message(TRANSPORTADORAS_RECUPERADAS)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

}
