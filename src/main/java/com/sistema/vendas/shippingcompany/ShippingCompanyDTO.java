package com.sistema.vendas.shippingcompany;

import java.time.LocalDate;

import org.modelmapper.ModelMapper;

import com.sistema.vendas.address.Address;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 09/12/2021
 */

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ShippingCompanyDTO{

	private Long id;
	private String board;
	private String enrollment;
	private String name;
	private LocalDate registrationDate;
	private String cnpj;
	private String email;
	private Address address;
	private String phoneNumberOne;
	private String phoneNumberTwo;
	private String comments;

	public static ShippingCompanyDTO convertToDto(ShippingCompany shippingCompany){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(shippingCompany, ShippingCompanyDTO.class);
	}

	public static ShippingCompany convertToEntity(ShippingCompanyDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, ShippingCompany.class);
	}

}
