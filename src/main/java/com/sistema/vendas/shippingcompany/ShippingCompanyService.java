package com.sistema.vendas.shippingcompany;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 09/12/2021
 */

public interface ShippingCompanyService{

	List<ShippingCompany> findAll();

	Optional<ShippingCompany> findById(Long id);

	ShippingCompany save(ShippingCompany shippingCompany);

	ShippingCompany update(ShippingCompany shippingCompany);

	void delete(Long id);

	List<ShippingCompany> findByNameStartingWithIgnoreCase(String name);

	Optional<ShippingCompany> findByCnpjLike(String cnpj);

}
