package com.sistema.vendas.role;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(propagation = Propagation.SUPPORTS, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class RoleServiceImpl implements RoleService{

	private final RoleRepository repository;

	@Override
	public Role save(Role role){
		return repository.save(role);
	}

	@Override
	public Role update(Role role){
		return repository.save(role);
	}

	@Override
	public Optional<Role> delete(Long id){
		Optional<Role> role = repository.findById(id);
		if(role.isPresent()){
			repository.deleteById(id);
			return role;
		}
		return Optional.empty();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> findAll(){
		log.info("Get all roles");
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Role> findById(Long id){
		log.info("Get role id: {}.", id);
		return repository.findById(id);
	}

}
