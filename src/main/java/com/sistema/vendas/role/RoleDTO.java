package com.sistema.vendas.role;

import org.modelmapper.ModelMapper;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class RoleDTO{

	private Long id;
	private String name;

	public static RoleDTO convertToDto(Role role){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(role, RoleDTO.class);
	}

	public static Role convertToEntity(RoleDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, Role.class);
	}

}
