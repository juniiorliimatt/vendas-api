package com.sistema.vendas.provider;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.br.CNPJ;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sistema.vendas.address.Address;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 07/12/2021
 */

@Data
@Entity(name = "providers")
@NoArgsConstructor
@AllArgsConstructor
public class Provider implements Serializable{

	private static final long serialVersionUID = 7298182256957042023L;

	@Id
	@SequenceGenerator(name = "provider_sequence", sequenceName = "provider_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "provider_sequence")
	@Column(updatable = false)
	private Long id;

	@Column(unique = true, nullable = false)
	@NotEmpty(message = "Insira uma Razão Social")
	private String corporateName;

	@Column(nullable = false)
	@NotEmpty(message = "Insira um Nome Fantasia")
	private String fantasyName;

	@CNPJ
	@Column(unique = true, nullable = false)
	@NotEmpty(message = "Insira um Nome Fantasia")
	private String cnpj;

	@Column(unique = true, nullable = false)
	@NotEmpty(message = "Insira uma Conta Corrente")
	private String checkingAccount;

	@Column(nullable = false)
	@NotEmpty(message = "Insira um Representante")
	private String representative;

	@Column(unique = true, nullable = false)
	@NotEmpty(message = "Insira uma Inscrição")
	private String enrollment;

	@Column(nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate foundationDate;

	private String comments;

	@Email
	@Column(unique = true)
	@NotEmpty(message = "Insira um email")
	private String email;

	@Column(unique = true)
	@NotEmpty(message = "Insira um número válido")
	private String phoneNumberOne;

	@Column(nullable = false)
	private String phoneNumberTwo;

	@Embedded
	@Valid
	private Address address;

}
