package com.sistema.vendas.provider;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 07/12/2021
 */

public interface ProviderService{

	List<Provider> findAll();

	Optional<Provider> findById(Long id);

	Provider save(Provider provider);

	Provider update(Provider provider);

	void delete(Long id);

}
