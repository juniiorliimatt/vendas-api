package com.sistema.vendas.provider;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 07/12/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class ProviderServiceImpl implements ProviderService{

	private final ProviderRepository repository;

	@Override
	@Transactional(readOnly = true)
	public List<Provider> findAll(){
		return repository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Optional<Provider> findById(Long id){
		return repository.findById(id);
	}

	@Override
	public Provider save(Provider provider){
		return repository.save(provider);
	}

	@Override
	public Provider update(Provider provider){
		return repository.save(provider);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

}
