package com.sistema.vendas.employee;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sistema.vendas.personal.PersonalData;
import com.sistema.vendas.user.User;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

@Data
@Entity(name = "employees")
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeData implements Serializable{

	private static final long serialVersionUID = -5439161538589823499L;

	@ApiModelProperty(value = "Id da pessoa")
	@Id
	@SequenceGenerator(name = "employee_sequence", sequenceName = "employee_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_sequence")
	@Column(updatable = false)
	private Long id;

	@Column(nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate hiringDate;

	@NotEmpty(message = "Forneça o número da carteira de trabalho")
	@Column(unique = true, nullable = false)
	@Size(max = 15)
	private String workPermit;

	@NotNull(message = "Informe o percentual de venda")
	@Column(nullable = false)
	private Double salePercentage;

	@NotNull(message = "Informe o saldo de caixa")
	@Column(nullable = false)
	private BigDecimal cashBalance;

	@NotNull(message = "Informe o salário")
	@Column(nullable = false)
	private BigDecimal salary;

	@NotEmpty(message = "Informe o cargo do funcionário")
	@Column(nullable = false)
	private String office;

	@Embedded
	@Valid
	private PersonalData personalData;

	@OneToOne(cascade = CascadeType.ALL)
	private User user;

	public User getUser(){
		return user;
	}

	public void setUser(User user){
		this.user = user;
	}

}
