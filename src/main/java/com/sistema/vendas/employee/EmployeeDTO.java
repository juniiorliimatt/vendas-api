package com.sistema.vendas.employee;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.modelmapper.ModelMapper;

import com.sistema.vendas.personal.PersonalData;
import com.sistema.vendas.user.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public class EmployeeDTO{

	private Long id;
	private LocalDate hiringDate;
	private String workPermit;
	private Double salePercentage;
	private BigDecimal cashBalance;
	private BigDecimal salary;
	private String office;
	private PersonalData personalData;
	private User user;

	public static EmployeeDTO convertToDto(EmployeeData employeeData){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(employeeData, EmployeeDTO.class);
	}

	public static EmployeeData convertToEntity(EmployeeDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, EmployeeData.class);
	}

}
