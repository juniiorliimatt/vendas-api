package com.sistema.vendas.employee;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 29/11/2021
 */

public interface EmployeeService{

	List<EmployeeData> findAll();

	Optional<EmployeeData> findById(Long id);

	EmployeeData save(EmployeeData employeeData);

	EmployeeData update(EmployeeData employeeData);

	void delete(Long id);

	List<EmployeeData> findByPersonalData_NameStartingWithIgnoreCase(String name);

	Optional<EmployeeData> findByPersonalData_CpfLike(String cpf);

	List<EmployeeData> findByPersonalData_Birthday(LocalDate birthday);

	List<EmployeeData> findByPersonalData_RegistrationDate(LocalDate registrationDate);

	Optional<EmployeeData> findByPersonalData_Rg(String rg);

	List<EmployeeData> findByHiringDate(LocalDate hiringDate);

	List<EmployeeData> findByCashBalanceOrderByCashBalanceDesc(BigDecimal cashBalance);

}
