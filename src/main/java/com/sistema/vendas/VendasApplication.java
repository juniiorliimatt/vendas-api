package com.sistema.vendas;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@EnableSwagger2
@SpringBootApplication
public class VendasApplication{

	public static void main(String[] args){
		SpringApplication.run(VendasApplication.class, args);
	}

	@Bean()
	public PasswordEncoder passwordEncoder(){
		return new BCryptPasswordEncoder();
	}

	@Bean()
	public ModelMapper modelMapper(){
		return new ModelMapper();
	}

}
