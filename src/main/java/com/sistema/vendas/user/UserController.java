package com.sistema.vendas.user;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.management.relation.RoleNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Slf4j
public class UserController{

	private final UserService service;
	private static final String USER_RETRIEVED = "User retrieved";

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		List<User> users = service.findAll();
		List<UserDTO> userDTOS = new ArrayList<>();
		users.forEach(user->userDTOS.add(new UserDTO(user)));
		log.info("Buscando todos os usuarios");
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(userDTOS))
																			 .message("Users retrieved")
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	@GetMapping("/find/username/{username}")
	public ResponseEntity<Response> findByUsername(@PathVariable("username") String username){
		Optional<User> user = service.findByUsername(username);
		return getResponseResponseEntity(user);
	}

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		Optional<User> user = service.findById(id);
		return getResponseResponseEntity(user);
	}

	@PostMapping("/save")
	public ResponseEntity<Response> save(@RequestBody UserDTO dto){
		User user = UserDTO.convertToEntity(dto);
		log.info("Usuario convertido para entidade com sucesso");
		User save = service.save(user);
		UserDTO userDTO = new UserDTO(save);
		URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
																							.path("/api/v1/user/find/{id}")
																							.buildAndExpand(save.getId())
																							.toUri();
		log.info("Usuario salvo com sucesso");
		return ResponseEntity.created(location)
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(userDTO))
																			 .message(USER_RETRIEVED)
																			 .status(CREATED)
																			 .statusCode(CREATED.value())
																			 .build());
	}

	@PostMapping("/add/role/to/user")
	public ResponseEntity<RoleToUserForm> addRoleToUser(@RequestBody RoleToUserForm role) throws RoleNotFoundException{
		service.addRoleToUser(role.getUsername(), role.getRoleName());
		log.info("Role {} adicionado ao usuario {}", role.getRoleName(), role.getUsername());
		return ResponseEntity.ok()
												 .build();
	}

	@PutMapping("/update")
	public ResponseEntity<Response> update(@RequestBody UserDTO dto){
		User user = UserDTO.convertToEntity(dto);
		User updated = service.update(user);
		UserDTO userDTO = new UserDTO(updated);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(userDTO))
																			 .message(USER_RETRIEVED)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	@DeleteMapping("/delete/id/{id}")
	public ResponseEntity<Response> deleteById(@PathVariable("id") Long id){
		service.delete(id);
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .message(USER_RETRIEVED)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	@GetMapping("/token/refresh")
	public ResponseEntity<Response> refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException{
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(service.refreshToken(request, response)))
																			 .message("Token Refresh")
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

	private ResponseEntity<Response> getResponseResponseEntity(Optional<User> user){
		if(user.isEmpty()){
			log.warn("Usuario não encontrado");
			return ResponseEntity.notFound()
													 .build();
		}
		UserDTO userDTO = new UserDTO(user.get());
		log.info("Usuario {} encontrado", user.get()
																					.getUsername());
		return ResponseEntity.ok()
												 .body(Response.builder()
																			 .timeStamp(Utils.formatDate())
																			 .data(List.of(userDTO))
																			 .message(USER_RETRIEVED)
																			 .status(OK)
																			 .statusCode(OK.value())
																			 .build());
	}

}
