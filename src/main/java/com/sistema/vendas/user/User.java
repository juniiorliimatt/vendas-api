package com.sistema.vendas.user;

import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.SEQUENCE;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.sistema.vendas.role.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Data
@Entity(name = "users")
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable{

	private static final long serialVersionUID = -4226531658964287270L;

	@Id
	@SequenceGenerator(name = "user_sequence", sequenceName = "user_sequence", allocationSize = 1)
	@GeneratedValue(strategy = SEQUENCE, generator = "user_sequence")
	@Column(updatable = false)
	private Long id;

	@Email
	@Column(unique = true, nullable = false)
	@NotEmpty(message = "Insira um email")
	private String username;

	@Column(nullable = false)
	@Length(min = 5, max = 100)
	@NotEmpty(message = "Insira uma senha")
	private String password;

	@ManyToMany(cascade = CascadeType.PERSIST, fetch = EAGER)
	private Collection<Role> roles = new ArrayList<>();

	public User(Long id, String username, String password){
		this.id = id;
		this.username = username;
		this.password = password;
	}

}
