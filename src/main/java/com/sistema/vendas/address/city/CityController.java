package com.sistema.vendas.address.city;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sistema.vendas.response.Response;
import com.sistema.vendas.utils.Utils;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 27/11/2021
 */

@RestController
@RequestMapping("/api/v1/city")
@RequiredArgsConstructor
public class CityController{

	private final CityService service;

	@GetMapping("/find/id/{id}")
	public ResponseEntity<Response> findById(@PathVariable("id") Long id){
		return getResponse(service.findById(id));
	}

	@GetMapping("/find/all")
	public ResponseEntity<Response> findAll(){
		return getResponse(service.findAll());
	}

	@GetMapping("/find/name/{name}")
	public ResponseEntity<Response> findByNameStartingWithIgnoreCase(@PathVariable("name") String name){
		return getResponse(service.findByNameStartingWithIgnoreCase(name));
	}

	@GetMapping("/find/uf/{uf}")
	public ResponseEntity<Response> findByUfIgnoreCase(@PathVariable("uf") String uf){
		return getResponse(service.findByUfIgnoreCase(uf));
	}

	private ResponseEntity<Response> getResponse(Optional<City> city){
		if(city.isEmpty()){
			return new ResponseEntity<>(Response.builder()
																					.timeStamp(Utils.formatDate())
																					.message("Nenhuma cidade encontrada")
																					.status(HttpStatus.NOT_FOUND)
																					.statusCode(HttpStatus.NOT_FOUND.value())
																					.build(), HttpStatus.NOT_FOUND);
		}

		CityDTO cityDTO = CityDTO.convertToDto(city.get());
		cityDTO.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(CityController.class)
																													.findAll())
																 .withRel("Lista de Cidades"));

		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.data(List.of(cityDTO))
																				.message("Cidade recuperada")
																				.status(HttpStatus.OK)
																				.statusCode(HttpStatus.OK.value())
																				.build(), HttpStatus.OK);
	}

	private ResponseEntity<Response> getResponse(List<City> cities){
		List<CityDTO> cityDTOs = new ArrayList<>();
		cities.forEach(city->cityDTOs.add(CityDTO.convertToDto(city)));
		cityDTOs.forEach(cityDto->cityDto.add(WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(CityController.class)
																																										.findById(cityDto.getId()))
																													 .withSelfRel()));
		return new ResponseEntity<>(Response.builder()
																				.timeStamp(Utils.formatDate())
																				.data(cityDTOs)
																				.message("Cidades Recuperada")
																				.status(HttpStatus.OK)
																				.statusCode(HttpStatus.OK.value())
																				.build(), HttpStatus.OK);
	}

}
