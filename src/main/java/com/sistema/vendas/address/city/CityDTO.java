package com.sistema.vendas.address.city;

import org.modelmapper.ModelMapper;
import org.springframework.hateoas.RepresentationModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 27/11/2021
 */

@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class CityDTO extends RepresentationModel<CityDTO>{

	private Long id;
	private String uf;
	private String name;

	public static City convertToEntity(CityDTO dto){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(dto, City.class);
	}

	public static CityDTO convertToDto(City city){
		ModelMapper modelMapper = new ModelMapper();
		return modelMapper.map(city, CityDTO.class);
	}

}
