package com.sistema.vendas.address.city;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

@Service
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.SERIALIZABLE, timeout = 30)
public class CityServiceImpl implements CityService{

	private final CityRepository repository;

	@Override
	@Transactional(readOnly = true)
	public Optional<City> findById(Long id){
		return repository.findById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<City> findByNameStartingWithIgnoreCase(String name){
		return repository.findByNameStartingWithIgnoreCase(name);
	}

	@Override
	@Transactional(readOnly = true)
	public List<City> findByUfIgnoreCase(String uf){
		return repository.findByUfIgnoreCase(uf);
	}

	@Override
	@Transactional(readOnly = true)
	public List<City> findAll(){
		return repository.findAll();
	}

	@Override
	public City save(City city){
		return repository.save(city);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

}
