package com.sistema.vendas.address.state;

import java.util.List;
import java.util.Optional;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 24/11/2021
 */

public interface StateService{

	Optional<State> findById(Long id);

	Optional<State> findByUfIgnoreCase(String uf);

	List<State> findByNameStartingWithIgnoreCase(String name);

	List<State> findAll();

	State save(State state);

	void delete(Long id);

}
