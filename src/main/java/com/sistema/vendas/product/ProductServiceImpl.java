package com.sistema.vendas.product;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 18/12/2021
 */

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService{

	private final ProductRepository repository;

	@Override
	public List<Product> findAll(){
		return repository.findAll();
	}

	@Override
	public Optional<Product> findById(Long id){
		return repository.findById(id);
	}

	@Override
	public Product save(Product product){
		return repository.save(product);
	}

	@Override
	public Product update(Product product){
		return repository.save(product);
	}

	@Override
	public void delete(Long id){
		repository.deleteById(id);
	}

}
