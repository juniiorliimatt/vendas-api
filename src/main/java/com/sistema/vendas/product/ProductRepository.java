package com.sistema.vendas.product;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 18/12/2021
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

}
