package com.sistema.vendas.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Utils{

	private Utils(){
	}

	public static String formatDate(){
		LocalDateTime now = LocalDateTime.now();
		return now.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
	}

}
