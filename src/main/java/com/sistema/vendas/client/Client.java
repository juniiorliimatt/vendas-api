package com.sistema.vendas.client;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.Valid;

import com.sistema.vendas.personal.PersonalData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Junior Lima
 * @version 1.0
 * @since 05/12/2021
 */

@Data
@Entity(name = "customers")
@NoArgsConstructor
@AllArgsConstructor
public class Client implements Serializable{

	private static final long serialVersionUID = -7360690221621377862L;

	@Id
	@SequenceGenerator(name = "client_sequence", sequenceName = "client_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_sequence")
	@Column(updatable = false)
	private Long id;

	@Embedded
	@Valid
	private PersonalData personalData;

}
